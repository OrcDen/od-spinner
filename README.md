# <od-spinner>

> Spinning component to indicate busy or loading

`<od-spinner>` is a simple component that will hijack the users experience and display a spinning loading cirlce instead.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-spinner
```
## Usage
```
import '@orcden/od-spinner';
```
```
<div style='height: 500px; position: relative;'>
    <od-spinner scoped size='100' bar-size='10' id='spinner'></od-spinner>
</div>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `scoped`  | Boolean  | false  | Set this so that the spinner doesn't take up the whole screen. Will scope itslef to the next parent element with position relative (in CSS)  |
| `active`  | Boolean | false   | Controls CSS to show/hide the spinner |
| `size`  | Number | 40 | In pixels. Controls the width ( diameter ) of the spinner |
| `bar-size`  | Number | 3 | In pixels. Controls the stroke width of the spinner |

## Properties
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `scoped`  | Boolean  | false  | Set this so that the spinner doesn't take up the whole screen. Will scope itslef to the next parent element with position relative (in CSS)  |
| `active`  | Boolean | false   | Controls CSS to show/hide the spinner |
| `size`  | Number | 40 | In pixels. Controls the width ( diameter ) of the spinner |
| `barSize`  | Number | 3 | In pixels. Controls the stroke width of the spinner |

## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `toggleActive`   | None | toggles the spinner on and off |

## Styling
- CSS variables are available to alter the default styling provided

| Shadow Parts     | Description           |
|------------------|-----------------------|
| modal           | The modal inside the component |
| circle           | The circle HTML Element inside the component. Set Stroke to change the colour. |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```