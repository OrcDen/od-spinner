import '@orcden/od-modal';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>
        :host {
            height: 100%;
            width: 100%;
            display: inline-block;
            transition: all 500ms linear;
        }

        :host( :not( [active] ) ) {
            opacity: 0;
            display: none;
        }

        circle {
            stroke: #2196F3;
            transition: stroke-dashoffset 800ms linear;
            fill: transparent;
        }

        svg {
            animation: spin 1s infinite linear;
            transform-origin: 50% 50%;

            box-shadow: none !important;
            -webkit-box-shadow: none !important;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }        

    </style>
    
    <od-modal id="modal" hijack="true" exportparts='modal'>
        <svg id="svg">
            <circle id="circle" part='circle'></circle>
        </svg>
    </od-modal> 
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-spinner" );

export class OdSpinner extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        //set attribute default values first
        if ( !this.hasAttribute( "size" ) ) {
            this.setAttribute( "size", 40 );
        }
        if ( !this.hasAttribute( "bar-size" ) ) {
            this.setAttribute( "bar-size", 3 );
        }

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( "size" );
        this._upgradeProperty( "bar-size" );
        this._upgradeProperty( "scoped" );
        this._upgradeProperty( "active" );

        //listeners and others etc.
        this.__progress = 0;

    }

    disconnectedCallback() {
        clearInterval( this.__animation );
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["size", "bar-size", "active", "scoped"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "size": {
                if ( !this._validateNumber( newValue ) ) {
                    this.setAttribute( "size", oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._inputChanged( this.active, this._progress, this.size, this.barSize );
                }
                break;
            }
            case "bar-size": {
                if ( !this._validateNumber( newValue ) ) {
                    this.setAttribute( "bar-size", oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._inputChanged( this.active, this._progress, this.size, this.barSize );
                }
                break;
            }
            case "active": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setActiveAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setActiveAttribute( newValue );
                    this._setModalActive( newValue );
                    this._inputChanged( this.active, this._progress, this.size, this.barSize );
                }
                break;
            }
            case "scoped": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setScopedAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setScopedAttribute( newValue );
                    this._setModalScoped( newValue );
                }
                break;
            }
        }
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get active() {
        return this.hasAttribute( "active" );
    }

    set active( isActive ) {
        if ( typeof isActive !== "boolean" ) {
            return;
        }
        this._setActiveAttribute( isActive );
    }

    _setActiveAttribute( newV ) {
        this._setBooleanAttribute( 'active', newV );
    }

    get scoped() {
        return this.hasAttribute( "scoped" );
    }

    set scoped( isActive ) {
        if ( typeof isActive !== "boolean" ) {
            return;
        }
        this._setScopedAttribute( isActive );
    }

    _setScopedAttribute( newV ) {
        this._setBooleanAttribute( 'scoped', newV );
    }

    get size() {
        return parseInt( this.getAttribute( "size" ) );
    }

    set size( newSize ) {
        if ( typeof newSize !== "number" ) {
            return;
        }
        if ( this._validateNumber( newSize ) ) {
            this.setAttribute( "size", newSize );
        }
    }

    get barSize() {
        return parseInt( this.getAttribute( "bar-size" ) );
    }

    set barSize( newSize ) {
        if ( typeof newSize !== "number" ) {
            return;
        }
        if ( this._validateNumber( newSize ) ) {
            this.setAttribute( "bar-size", newSize );
        }
    }

    get _progress() {
        return this.__progress;
    }

    set _progress( progress ) {
        this.__progress = progress;
        this._inputChanged( this.active, this._progress, this.size, this.barSize );
    }

    _validateNumber( newV ) {
        let temp = parseInt( newV );
        return !isNaN( temp ) && temp >= 0;
    }

    _setModalActive( active ) {
        this.shadowRoot.querySelector( '#modal' ).active = active === 'true';
    }

    _setModalScoped( scoped ) {
        this.shadowRoot.querySelector( '#modal' ).scoped = scoped === 'true';
    }

    _inputChanged( active, progress, size, barSize ) {
        if( isNaN( size ) || isNaN( barSize ) ) {
            return;
        }
        if ( !active ) {
            progress = 0;
        }
        var diameter = size - 2 * barSize;
        var circumference = diameter * Math.PI;

        this.__circle = this.shadowRoot.querySelector( '#circle' );
        var svg = this.shadowRoot.querySelector( '#svg' );

        svg.style.height = svg.style.width = size + "px";
        this.__circle.style.strokeWidth = barSize + "px";
        this.__circle.setAttribute('r', diameter / 2 + "px");
        var halfsize = size / 2 + "px";
        this.__circle.setAttribute('cx', halfsize);
        this.__circle.setAttribute('cy', halfsize);

        if (progress == 0) {
            progress = .3;
            this.__animateSmall = false;
            this.__animation = setInterval(function () {
                if (this.__animateSmall) {
                    this.__circle.style.strokeDashoffset = circumference * (1 - .3) + "px";
                }
                else {
                    this.__circle.style.strokeDashoffset = circumference * (1 - .7) + "px";
                }
                this.__animateSmall = !this.__animateSmall;
            }.bind(this), 1000);
        }
        else {
            clearInterval(this.__animation);
            if (progress < 0) {
                progress = 0;
            }
            else if (progress > 1) {
                progress = 1;
            }
        }
        this.__circle.style.strokeDasharray = circumference + "px";
        this.__circle.style.strokeDashoffset = circumference * (1 - progress) + "px";
    }

    toggleActive() {
        this.active = !this.active;
    }
}
