export default {
    testServer: 'http://localhost:8079',
    name: 'OD Top Button',
    componentTag: 'od-top-button',
    componentSelector: '#top-button',
    hasShadow: true,
    shadowSelector: '#top-button',
    properties: [
        {
            name: 'active',
            type: Boolean,
            default: false
        },
        {
            name: 'offset',
            type: Number,
            default: 1,
            validValue: 5
        }
    ]
}